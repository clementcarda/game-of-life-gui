from time import sleep
from tkinter import *
from tkinter import ttk
from Board import Board


class Interface:
    def __init__(self, root):
        self._row = 15
        self._column = 15
        self._speed = 1000
        self._active = False
        self._list_buttons = []
        self._board = Board
        self.parent = root
        self._b_play = ttk.Button
        self._b_stop = ttk.Button
        self._mid_speed = ttk.Button
        self._norm_speed = ttk.Button
        self._double_speed = ttk.Button
        self._b_resize = ttk.Button

        self.parent.title("Game of Life")

        self.mainframe = ttk.Frame(root, padding="3 3 3 3")
        self.mainframe.grid(column=0, row=0, sticky=(N, S, E, W))

        # Create Widgets
        buttonframe = ttk.Frame(self.mainframe, padding="3 3 3 3")
        buttonframe.grid(column=0, row=0, sticky=(E,W))

        self.build_buttons_frame(buttonframe)

        self._boardframe = ttk.Frame(self.mainframe)
        self._boardframe.grid(column=0, row=1, sticky=(N, S, E, W))
        self.build_grid_frame(self._boardframe, self._row, self._column)



        root.columnconfigure(0, weight=1)
        root.rowconfigure(0, weight=1)
        self.mainframe.columnconfigure(0, weight=1)
        self.mainframe.rowconfigure(0, weight=1)
        self.mainframe.rowconfigure(1, weight=10)

        #self._play_button['command'] = lambda: self.play_button()
        self.checkout_grid()



    def build_buttons_frame(self, frame):
        self._b_play = ttk.Button(frame, text='play', command=self.play_button)
        self._b_play.grid(column=1, row=0, sticky=E)
        self._b_stop = ttk.Button(frame, text='pause', command=self.stop_button, state=DISABLED)
        self._b_stop.grid(column=2, row=0, sticky=W)

        self._mid_speed = ttk.Button(frame, text='0.5x', state=NORMAL)
        self._mid_speed['command'] = lambda s = 2000: self.change_speed(s)
        self._mid_speed.grid(column=3, row=0, sticky=E)

        self._norm_speed = ttk.Button(frame, text='1x', state=DISABLED)
        self._norm_speed['command'] = lambda s=1000: self.change_speed(s)
        self._norm_speed.grid(column=4, row=0, sticky=E)

        self._double_speed = ttk.Button(frame, text='2x', state=NORMAL)
        self._double_speed['command'] = lambda s=500: self.change_speed(s)
        self._double_speed.grid(column=5, row=0, sticky=E)

        self._b_resize = ttk.Button(frame, text='resize', command=self.resize_panel)
        self._b_resize.grid(column=6, row=0, sticky=E)


    def build_grid_frame(self, frame, grid_row, grid_column):
        self._board = Board(grid_row, grid_column)

        self._list_buttons = [
            [Button(frame, bg='white', width=1, height=1) for _ in range(grid_column)]
            for _ in range(grid_row)
        ]

        for row in range(grid_row):
            for column in range(grid_column):
                self._list_buttons[row][column].grid(row=row, column=column, sticky=(E, W))
                self._list_buttons[row][column]['command'] = lambda row=row, column=column: self.cell_toggle(
                    self._list_buttons[row][column], row, column
                )
    def cell_toggle(self, button, row, column):
        cell = self._board.get_cell(row, column)
        cell.change_state()
        if cell.is_alive():
            button['bg'] = 'black'
        else:
            button['bg'] = 'white'

    def checkout_grid(self):
        for row in range(self._row):
            for column in range(self._column):
                if self._board.get_cell(row, column).is_alive():
                    self._list_buttons[row][column]['bg'] = 'black'
                else:
                    self._list_buttons[row][column]['bg'] = 'white'

    def run(self):
        if self._active:
            self._board.update_board()
            self.checkout_grid()
            self.parent.after(self._speed, self.run)

    def play_button(self):
        self._active = True
        self._b_play['state'] = DISABLED
        self._b_stop['state'] = NORMAL
        self.run()

    def stop_button(self):
        self._active = False
        self._b_play['state'] = NORMAL
        self._b_stop['state'] = DISABLED

    def change_speed(self, speed):
        self._speed = speed
        if speed == 2000:
            self._mid_speed['state'] = DISABLED
            self._norm_speed['state'] = NORMAL
            self._double_speed['state'] = NORMAL
        elif speed == 1000:
            self._mid_speed['state'] = NORMAL
            self._norm_speed['state'] = DISABLED
            self._double_speed['state'] = NORMAL
        elif speed == 500:
            self._mid_speed['state'] = NORMAL
            self._norm_speed['state'] = NORMAL
            self._double_speed['state'] = DISABLED

    def resize_panel(self):
        self._newframe = Toplevel(self.parent)
        row_label = Label(self._newframe, text='rows')
        row_label.grid(column=0, row=0)
        self._spin_row_value = Spinbox(self._newframe, from_=0, to=50)
        self._spin_row_value.grid(column=2, row=0)

        col_label = Label(self._newframe, text='column')
        col_label.grid(column=0, row=1)
        self._spin_col_value = Spinbox(self._newframe, from_=0, to=50)
        self._spin_col_value.grid(column=2, row=1)

        b_ok = ttk.Button(self._newframe, text='ok', command=self.resize)
        b_ok.grid(column=1, row=2)

    def resize(self):
        self._row = int(self._spin_row_value.get())
        self._column = int(self._spin_col_value.get())

        self._boardframe = ttk.Frame(self.mainframe)
        self._boardframe.grid(column=0, row=1, sticky=(N, S, E, W))

        self.build_grid_frame(self._boardframe, self._row, self._column)
        self.checkout_grid()

        self._newframe.destroy()

    def print_spins_values(self):
        row = self.row_value.get()
        print(row)

