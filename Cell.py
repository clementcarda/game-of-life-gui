
class Cell:
    def __init__(self):
        self._alive = False

    def __repr__(self):
        if self._alive:
            return "0"
        else:
            return "·"

    def change_state(self):
        if self._alive:
            self._alive = False
        else:
            self._alive = True

    def set_state(self, state: bool):
        self._alive = state

    def is_alive(self):
        return self._alive
