from random import randint
from Cell import Cell


class Board:
    def __init__(self, row, column, pop_rate=0):
        self._row = row
        self._column = column
        self._grid = [[Cell() for _ in range(column)] for _ in range(row)]
        self.populate(pop_rate)

    def __repr__(self):
        pr = '\n'*10

        for row in self._grid:
            for cell in row:
                pr += cell.__repr__()
            pr += '\n'
        return pr

    def populate(self, pop_rate):
        for row in self._grid:
            for cell in row:
                chance_number = randint(1, 100)
                if chance_number <= pop_rate:
                    cell.set_state(True)
                else:
                    cell.set_state(False)

    def get_cell(self, row, column):
        return self._grid[row][column]

    def get_neighbours(self, cell_row, cell_column):
        neighbours_list = []

        for check_row in range(-1, 2):
            for check_column in range(-1, 2):

                # check if neighbour is valid (not himself or out of board)
                valid_neighbour = True
                n_row = cell_row + check_row
                n_column = cell_column + check_column

                if n_row == cell_row and n_column == cell_column:
                    valid_neighbour = False
                elif n_row < 0 or n_row >= self._row:
                    valid_neighbour = False
                elif n_column < 0 or n_column >= self._column:
                    valid_neighbour = False

                if valid_neighbour:
                    neighbours_list.append(self._grid[n_row][n_column])

        return neighbours_list

    def update_board(self):
        goes_change = []
        for row in range(self._row):
            for column in range(self._column):
                neighbours_list = self.get_neighbours(row, column)
                current_cell = self.get_cell(row, column)

                alive_neighbours = sum(c.is_alive() for c in neighbours_list)
                if current_cell.is_alive():
                    if alive_neighbours < 2 or alive_neighbours > 3:
                        goes_change.append(current_cell)
                elif alive_neighbours == 3:
                    goes_change.append(current_cell)

        for cell in goes_change:
            cell.change_state()
